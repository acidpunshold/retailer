<?php
$activeFlag = true;
?>

<div class="globalform wrap headerpopup">
  <div class="popup-close">

    Закрыть окно
    <svg class="close-svg">
      <use xlink:href="img/icons/close.svg#close-svg"></use>
    </svg>
  </div>
  <div class="wrap">
    <div class="check-wrap">
      <div class="check scroll">
        <div class="title">выберите ритейлеров, на переговоры с которыми вы хотели бы попасть</div>
        <div class="tabs">
          <ul class="tabs__caption check__caption-items">
              <?php foreach ($retailers as $retailer): ?>
                  <li class="check__caption-item item--fashion <?= $activeFlag ? 'active' : '' ; ?>"> <span><?= $retailer->name ?></span></li>
                  <?php $activeFlag = false ?>
              <?php endforeach; ?>
          </ul>
          <div class="double-arrows">
            <div class="double-arrows--wrap"></div>
          </div>
          <div class="check__content-items checkbox">
              <?php $activeFlag = true; ?>
              <?php foreach ($retailers as $retailer): ?>
                 <div class="tabs__content <?= $activeFlag ? 'active' : '' ; ?> check__content-item">
                  <?php foreach ($retailer->companies as $key => $company): ?>
                        <div class="checkbox__item">
                          <input type="checkbox" name="<?= $company->name ?>" value="<?= $company->name ?>">
                          <label for="<?= $company->name ?>"><?= $company->name ?><a href="<?= $company->site ?>" class="adress"><?= $company->site ?></a></label>
                        </div>
                  <?php endforeach; ?>
                </div>
                <?php $activeFlag = false ?>
              <?php endforeach; ?>

          </div>
        </div>
        <button class="btn btn--transparent none-desctop">Cохранить список</button>
      </div>
    </div>
    <div class="choosen">
      <div class="title">Выбранные</div>
      <ul class="choosen__items scroll">
        <li class="choosen__item">Familia<span class="cross"></span></li>
        <li class="choosen__item">Обувь для всей семьи<span class="cross"></span></li>
        <li class="choosen__item">Ralf Ringer<span class="cross"></span></li>
        <li class="choosen__item">Jack Wolfskin<span class="cross"></span></li>
        <li class="choosen__item">34PLAY<span class="cross"></span></li>
        <li class="choosen__item">Incanto<span class="cross"></span></li>
        <li class="choosen__item">Алеф<span class="cross"></span></li>
        <li class="choosen__item">Griol<span class="cross"></span></li>
        <li class="choosen__item">АЛФАВИТ<span class="cross"></span></li>
        <li class="choosen__item">Zenden<span class="cross"></span></li>
      </ul>
      <div class="double-arrows">
        <div class="double-arrows--wrap"></div>
      </div>
      <button class="btn btn--transparent none-mob">Cохранить список</button>
    </div>
  </div>
</div>
