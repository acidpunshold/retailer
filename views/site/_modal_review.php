<?php foreach ($reviews as $key => $review): ?>
    <div class="globalform wrap scroll reviewspopup reviewspopup<?= $key ?>">
      <div class="popup-close">

        Закрыть окно
        <svg class="close-svg">
          <use xlink:href="img/icons/close.svg#close-svg"></use>
        </svg>
      </div>
      <div class="wrap">
        <div class="popup-review scroll">
          <div class="popup-review__top">
            <div class="popup-review__name"><?= $review['name'] ?></div>
            <div class="popup-review__foto"><img src="<?= $review['img'] ?>" alt="woman"></div>
            <div class="popup-review__descr">
              <div class="reviews-item__company"><?= $review['company'] ?></div>
              <div class="reviews-item__position"><?= $review['position'] ?></div>
            </div>
            <div class="popup-review__date">
              <svg class="calendar">
                <use xlink:href="img/icons/calendar.svg#calendar"> </use>
              </svg>
              <p> <span>Дата посещения: </span>05.06.2018</p>
            </div>
          </div>
          <div class="popup-review__bottom">
            <div class="tabs">
              <ul class="tabs__caption popup-review__caption">
                <li class="review-item active"> <span>Видео</span></li>
                <li class="review-item"> <span>Текст</span></li>
              </ul>
              <div class="tabs__content popup-review__content active">
                <div class="popup-review__video">
                  <iframe width="560" height="315" src="https://www.youtube.com/embed/8leorFMy0rg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                </div>
              </div>
              <div class="tabs__content popup-review__content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<?php endforeach; ?>
