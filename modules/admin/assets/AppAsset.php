<?php

namespace app\modules\admin\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/main.css',
        'css/font-awesome.css',
    ];
    public $js = [
      'js/bootstrap.js',
      'js/modernizr-custom.js',
      'js/autosize.min.js',
      'js/jquery.nicescroll.min.js',
      'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
}
