<?php
namespace app\modules\admin\models;

use yii\base\Model;

class CompanyLinkForm extends Model
{
    public $companyId;

    public function rules()
    {
        return [
            [['companyId'], 'required'],
        ];
    }
    public function attributeLabels()
    {
        return [
            'companyId' => 'Компания',
        ];
    }
}
