<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "retailer".
 *
 * @property int $id
 * @property string $name
 *
 * @property RetailerCompany[] $retailerCompanies
 * @property Company[] $companies
 */
class Retailer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'retailer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRetailerCompanies()
    {
        return $this->hasMany(RetailerCompany::className(), ['retailer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['id' => 'company_id'])->viaTable('retailer_company', ['retailer_id' => 'id']);
    }
}
