<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\Retailer;
use app\modules\admin\models\RetailerSearch;
use app\modules\admin\models\Company;
use app\modules\admin\models\CompanyLinkForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

/**
 * RetailerController implements the CRUD actions for Retailer model.
 */
class RetailerController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'unlink-company' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Retailer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RetailerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Retailer model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $companiesProvider = new ArrayDataProvider([
            'allModels' => $model->companies,
            'sort' => [
                'attributes' => ['id', 'name', 'site'],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        return $this->render('view', [
            'model' => $model,
            'companiesProvider' => $companiesProvider,
        ]);
    }

    /**
     * Creates a new Retailer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Retailer();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Retailer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $companies = Company::find()
            ->select(['name'])
            ->indexBy('id')
            ->where(['not in', 'id', ArrayHelper::getColumn($model->companies, 'id')])
            ->asArray()
            ->column();

        $companyLink = new CompanyLinkForm();

        if ($companyLink->load(Yii::$app->request->post())) {
            $company = Company::findOne($companyLink->companyId);
            $model->link('companies', $company);
            return $this->redirect(['update', 'id' => $model->id]);
        }

        $companiesProvider = new ArrayDataProvider([
            'allModels' => $model->companies,
            'sort' => [
                'attributes' => ['id', 'name', 'site'],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        return $this->render('update', [
            'model' => $model,
            'companies' => $companies,
            'companyLink' => $companyLink,
            'companiesProvider' => $companiesProvider,
        ]);
    }

    /**
     * Deletes an existing Retailer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionUnlinkCompany($retailerId, $companyId)
    {
        $retailer = Retailer::findOne($retailerId);
        $company = Company::findOne($companyId);
        $retailer->unlink('companies', $company, true);
        return $this->redirect(['update', 'id' => $retailerId]);
    }

    /**
     * Finds the Retailer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Retailer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Retailer::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
