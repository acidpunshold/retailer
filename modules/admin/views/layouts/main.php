<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\modules\admin\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div id="ui" class="ui">

    <?= $this->render('header.php') ?>

    <?= $this->render('aside.php') ?>

    <!--main content start-->

    <div id="content" class="ui-content ui-content-aside-overlay">
        <div class="ui-content-body">
            <div class="ui-container">
                <?= $content ?>
            </div>
        </div>
    </div>
    <!--main content end-->

    <!--footer start-->
    <div id="footer" class="ui-footer">
        2019 &copy; Retailer (c)
    </div>
    <!--footer end-->

</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
