<?php

  use yii\helpers\Url;
  use yii\helpers\Html;

 ?>

<!--header start-->
<header id="header" class="ui-header">

    <div class="navbar-header">
        <!--logo start-->
        <a href="<?= Url::to(['/site/index'])?>" class="navbar-brand">

            <!-- сюда большую картинку -->
            <span class="logo">Retailer</span>

            <!-- сюда маленькую картинку -->
            <span class="logo-compact">Retailer</span>
        </a>
        <!--logo end-->
    </div>

    <div class="navbar-collapse nav-responsive-disabled">

        <!--toggle buttons start-->
        <ul class="nav navbar-nav">
            <li>
                <a class="toggle-btn" data-toggle="ui-nav" href="">
                    <i class="fa fa-bars"></i>
                </a>
            </li>
        </ul>
        <!-- toggle buttons end -->

        <!--notification start-->
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown dropdown-usermenu">
                <a href="#" class=" dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                    <div class="user-avatar"><img src="https://www.gravatar.com/avatar/" alt="..."></div>
                    <span class="hidden-sm hidden-xs">Миронов Алексей</span>
                    <!--<i class="fa fa-angle-down"></i>-->
                    <span class="caret hidden-sm hidden-xs"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                    <li><a href="<?= Url::to(['/settings/index'])?>"><i class="fa fa-cogs"></i>Настройки</a></li>
                    <li class="divider"></li>
                    <li><a href="<?= Url::to(['/site/logout'])?>" data-method="post"><i class="fa fa-sign-out"></i> Log Out</a></li>
                </ul>
            </li>
        </ul>
        <!--notification end-->

    </div>

</header>
<!--header end-->
