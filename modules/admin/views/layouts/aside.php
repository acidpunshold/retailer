<?php
// use Yii;
use yii\helpers\Url;

?>

<!--sidebar start-->
<aside id="aside" class="ui-aside">
    <ul class="nav" ui-nav>
        <li class="nav-head">
            <h5 class="nav-title text-uppercase light-txt">Навигация</h5>
        </li>
        <li>
            <a href="<?= Url::to(['/admin']) ?>"><i class="fa fa-home"></i><span>Главная</span></a>
            <a href="<?= Url::to(['/admin/company']) ?>"><i class="fa fa-money"></i><span>Компании</span></a>
            <a href="<?= Url::to(['/admin/retailer']) ?>"><i class="fa fa-qrcode"></i><span>Сегменты</span></a>
        </li>


    </ul>
</aside>
<!--sidebar end-->
