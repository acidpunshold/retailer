<?php
use yii\helpers\Html;
?>
<tr>
    <td><?= $model->name ?></td>
    <td><?= $model->site ?></td>
    <td><?= Html::a('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>', ['unlink-company', 'retailerId' => $retailerId, 'companyId' => $model->id], [
        'data' => [
            'method' => 'post',
        ],
    ]) ?></td>
</tr>
