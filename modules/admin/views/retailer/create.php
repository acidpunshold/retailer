<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Retailer */

$this->title = 'Добавить сегмент';
$this->params['breadcrumbs'][] = ['label' => 'Сегменты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="retailer-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
