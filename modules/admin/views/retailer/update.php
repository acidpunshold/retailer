<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Retailer */

$this->title = 'Изменить сегмент: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Сегменты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="retailer-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'companies' => $companies,
        'companyLink' => $companyLink,
        'companiesProvider' => $companiesProvider,
    ]) ?>

</div>
