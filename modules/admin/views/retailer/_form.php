<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model app\models\Retailer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="retailer-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php if (!$model->isNewRecord): ?>
        <h3>Компании сегмента</h3>
        <h5>Добавить компанию</h5>
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($companyLink, 'companyId')->dropDownList($companies, ['prompt' => 'Выбрать компанию']) ?>

        <div class="form-group">
            <?= Html::submitButton('Добавить', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
        <div class="company-item">
            <table class="table table-hover table-striped table-condensed">
            <?= ListView::widget([
                'dataProvider' => $companiesProvider,
                'itemView' => '_list_companies',
                'viewParams' => [
                    'retailerId' => $model->id,
                ]
            ]); ?>
            </table>
        </div>
    <?php endif; ?>
</div>
