<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model app\models\Retailer */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Retailers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="retailer-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить сегмент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

    <h3>Компании сегмента</h3>

    <div class="company-item">
        <table class="table table-hover table-striped table-condensed">
        <?= ListView::widget([
            'dataProvider' => $companiesProvider,
            'itemView' => '_list_companies',
            'viewParams' => [
                'retailerId' => $model->id,
            ]
        ]); ?>
        </table>
    </div>

</div>
