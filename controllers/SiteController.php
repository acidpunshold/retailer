<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\modules\admin\models\Retailer;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $reviews = [
            '1' => [
                'name' => 'Олег Семёнов',
                'company' => 'ООО "Кожанные шапки"',
                'position' => 'Главный помпон',
                'img' => 'img/reviews/man.jpg'
            ],
            '2' => [
                'name' => 'Ольга Семёнова',
                'company' => 'ЗАО "Сибирский Цирюльник"',
                'position' => 'Заместитель модника',
                'img' => 'img/reviews/woman.jpg'
            ],
            '3' => [
                'name' => 'Олег Семёнов',
                'company' => 'ООО "Кожанные шапки"',
                'position' => 'Главный помпон',
                'img' => 'img/reviews/man.jpg'
            ],
            '4' => [
                'name' => 'Ольга Семёнова',
                'company' => 'ЗАО "Сибирский Цирюльник"',
                'position' => 'Заместитель модника',
                'img' => 'img/reviews/woman.jpg'
            ],
            '5' => [
                'name' => 'Олег Семёнов',
                'company' => 'ООО "Кожанные шапки"',
                'position' => 'Главный помпон',
                'img' => 'img/reviews/man.jpg'
            ],
            '6' => [
                'name' => 'Ольга Семёнова',
                'company' => 'ЗАО "Сибирский Цирюльник"',
                'position' => 'Заместитель модника',
                'img' => 'img/reviews/woman.jpg'
            ],
        ];

        $retailers = Retailer::find()->all();

        return $this->render('index', [
            'reviews' => $reviews,
            'retailers' => $retailers,
        ]);
    }


}
