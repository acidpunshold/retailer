<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%retailer}}`.
 */
class m190204_131333_create_retailer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%retailer}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%retailer}}');
    }
}
