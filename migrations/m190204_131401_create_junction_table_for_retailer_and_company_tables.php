<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%retailer_company}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%retailer}}`
 * - `{{%company}}`
 */
class m190204_131401_create_junction_table_for_retailer_and_company_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%retailer_company}}', [
            'retailer_id' => $this->integer(),
            'company_id' => $this->integer(),
            'PRIMARY KEY(retailer_id, company_id)',
        ]);

        // creates index for column `retailer_id`
        $this->createIndex(
            '{{%idx-retailer_company-retailer_id}}',
            '{{%retailer_company}}',
            'retailer_id'
        );

        // add foreign key for table `{{%retailer}}`
        $this->addForeignKey(
            '{{%fk-retailer_company-retailer_id}}',
            '{{%retailer_company}}',
            'retailer_id',
            '{{%retailer}}',
            'id',
            'CASCADE'
        );

        // creates index for column `company_id`
        $this->createIndex(
            '{{%idx-retailer_company-company_id}}',
            '{{%retailer_company}}',
            'company_id'
        );

        // add foreign key for table `{{%company}}`
        $this->addForeignKey(
            '{{%fk-retailer_company-company_id}}',
            '{{%retailer_company}}',
            'company_id',
            '{{%company}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%retailer}}`
        $this->dropForeignKey(
            '{{%fk-retailer_company-retailer_id}}',
            '{{%retailer_company}}'
        );

        // drops index for column `retailer_id`
        $this->dropIndex(
            '{{%idx-retailer_company-retailer_id}}',
            '{{%retailer_company}}'
        );

        // drops foreign key for table `{{%company}}`
        $this->dropForeignKey(
            '{{%fk-retailer_company-company_id}}',
            '{{%retailer_company}}'
        );

        // drops index for column `company_id`
        $this->dropIndex(
            '{{%idx-retailer_company-company_id}}',
            '{{%retailer_company}}'
        );

        $this->dropTable('{{%retailer_company}}');
    }
}
